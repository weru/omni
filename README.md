### Where to find the *pages*?

All all page files will be found on the top level of **content** directory

The home page file is **content/_index.md**. If you need to edit the template that powers it, check **layouts/index.html**

The other ppages are powered by **layouts/_default/single.html** template

### What if I want to edit the menu?

To edit the menu, look inside the **config.toml** file. The top most navigation's menu links are found inside this file. They are specified like so

```
[[menu.main]]
		name = "Home"
		weight = -6
    url = "/"
[[menu.main]]
    name = "FAQ"
    weight = -5
    url = "/faq/"
```

### Where will I store my media assets?

Store all your images in the **static** folder. You may create additional subfolders as you go by; feel free to name them as you wish. 

To reference these files in your posts, link to the path where you stored the file, ommitting the word *static*.

For example, if I have a PNG image (example.png) inside a subfolder **images**, I would reference the image in my pages as

```markdown
![](/images/example.png)
```

Notice, I did not prepend */static/* on the file path.

### Deployment options

I would recommend using either

1. [Netlify cms](https://www.netlify.com/)
2. [Forestry.io](https://forestry.io)

Of course there are [several others](https://gohugo.io/hosting-and-deployment/), but from my experience, I find that these two automate the process for you.

With these options, you will:

* Make changes
* Commit
* Push to remote repo 

Once you push, netlify or forestry will detect you changes, build the site and make the changes live. Thus enabling you to concentrate on what is important (writing content).

Of course, you can have different branches e.g a test branch that syncs with netlify and a production branch that syncs with forestry.

For the demo, I used netlify ... that's why there is a netlify.toml config file in this repo

### Have further queries

Feel free to engage me. Have fun with Hugo!

Thanks.