---
title: "Documentation"
date: 2018-03-07T10:10:24-05:00
draft: false
---

## Parallel Programming with OmniThreadLibrary
 

I am writing a book about the OmniThreadLibrary called Parallel Programming with OmniThreadLibrary.

Read more about it on my blog.

## Articles and Presentations
### High-level threading
If you are new to multithreading and use at least Delphi 2009, you should start with my CodeRage 6 talk (video - CodeCentral, video - Dropbox, slides + transcript, code).  

Next in line are the articles on high-level threading constructs: Parallel.Async (another), Parallel.Future (exception handling), Parallel.Join, Parallel.ParallelTask, Parallel.ForEach, Parallel.Pipeline (exception handling), Parallel.ForkJoin, Parallel.BackgroundWorker, Parallel.TaskConfig.

You should also take a look at my articles dealing with multithreading problems from real life.

## Presentations
### ITDevCon 2010
FastMM in Depth – presentation (pps, pdf), code (zip)
Parallel Programming Made Easy – presentation (pps, pdf), code (zip)
Building Multithreaded Solutions with OmniThreadLibrary – presentation (pps, pdf), code (zip)

### VDUG webinar 2010
Parallel Programming with OmniThreadLibrary -  presentation (pdf), video (flash, mp4)

### ADUG 2011
Getting Full Speed with Delphi - presentation (pps), handouts (pdf), code (zip)

### ITDevCon 2011
Multithreading Made Simple with OmniThreadLibrary - presentation (pps, pdf), code (zip)

### ITDevCon 2012
Parallel Programming with OmniThreadLibrary - presentation (pps, pdf), code (zip)

## Low-level threading examples
The simplest possible OTL threading code

Hello, World example

How to (not) create a task

Task controller needs an owner

Bidirectional communication

Message-driven worker, the OTL way

Registering additional communication channels

Processing Windows messages

Background file scanning

Building a connection pool

Synchronize comes to OmniThreadLibrary

Alternatively, you can open all my OmniThreadLibrary related articles in one go.

OmniThreadLibrary internals
Task control interface

Communication subsystem

Lock-free stack tetralogy: Implementing lock-free stack, Lock-free stack problem, A working lock-free stack implementation, Lock-free queue

Containers

Calling OmniThreadLibrary methods by name

Using RTTI to call task methods

Blocking collection trilogy: Inverse semaphore, Dynamically allocated queue, Blocking collection

Parallel for implementation: Overview, Input, Output.

## Demos
### 0_Beep
The simplest possible OmniThreadLibrary threading code.

### 1_HelloWorld
Threaded "Hello, World" with TOmniEventMonitor component created in runtime.

### 2_TwoWayHello
Hello, World with bidirectional communication; TOmniEventMonitor created in runtime.

### 3_HelloWorld_with_package
Threaded "Hello, World" with TOmniEventMonitor component on the form.

### 4_TwoWayHello_with_package
Hello, World with bidirectional communication; TOmniEventMonitor component on the form.

### 5_TwoWayHello_without_loop
Hello, World with bidirectional communication, the OTL way.

### 6_TwoWayHello_with_object_worker
Obsolete, almost totally equal to the demo 5_TwoWayHello_without_loop.

### 7_InitTest
Demonstrates .WaitForInit, .ExitCode, .ExitMessage, and .SetPriority.

### 8_RegisterComm
Demonstrates creation of additional communication channels.

### 9_Communications
Simple communication subsystem tester.

### 10_Containers
Full-blown communication subsystem tester. Used to verify correctness of the lock-free code.

### 11_ThreadPool
Thread pool demo.

### 12_Lock
Demonstrates .WithLock.

### 13_Exceptions
Demonstrates exception catching.

### 14_TerminateWhen
Demonstrates .TerminateWhen and .WithCounter.

### 15_TaskGroup
Task group demo.

### 16_ChainTo
Demonstrates .ChainTo.

### 17_MsgWait
Demonstrates .MsgWait and Windows message processing inside tasks.

### 18_StringMsgDispatch
Calling task methods by name and address.

### 19_StringMsgBenchmark
Benchmarks various was of task method invokation.

### 20_QuickSort
Parallel QuickSort demo.

### 21_Anonymous_methods
Demonstrates the use of anonymous methods as task workers in Delphi 2009.

### 22_Termination
Tests for .Terminate and .Terminated.

### 23_BackgroundFileSearch
Demonstrates file scanning in a background thread.

### 24_ConnectionPool
Demonstrates how to create a connection pool with OmniThreadLibrary.

### 25_WaitableComm
Demo for ReceiveWait and SendWait.

### 26_MultiEventMonitor
How to run multiple event monitors in parallel.

### 27_RecursiveTree
Parallel tree processing.

### 28_Hooks
Demo for the new hook system.

### 29_ImplicitEventMonitor
Demo for OnMessage and OnTerminated, named method approach.

### 30_AnonymousEventMonitor
Demo for OnMessage and OnTerminated, anonymous method approach.

### 31_WaitableObjects
Demo for the new RegisterWaitObject/UnregisterWaitObject API.

### 32_Queue
Stress test for new TOmniBaseQueue and TOmniQueue.

### 33_BlockingCollection
Stress test for new TOmniBlockingCollection, also demoes the use of Environment to set process affinity.

### 34_TreeScan
Parallel tree scan using TOmniBlockingCollection.

### 35_ParallelFor
Parallel tree scan using Parallel.ForEach (Delphi 2009 and newer).

### 37_ParallelJoin
ParallelJoin: Parallel.Join demo.

### 38_OrderedFor
Ordered parallel for loops.

### 39_Future
Futures.

### 40_Mandelbrot
Very simple parallel graphics demo.

### 41_Pipeline
Multistage parallel processes.

### 42_MessageQueue
Stress test for TOmniMessageQueue.

### 43_InvokeAnonymous
Demo for IOmniTask.Invoke.

### 44_Fork-Join QuickSort
QuickSort implemented using Parallel.ForkJoin.

### 45_Fork-Join max
Max(array) implemented using Parallel.ForkJoin.

### 46_Async
Demo for Parallel.Async.

### 47_TaskConfig
Demo for task configuration with Parallel.TaskConfig.

### 48_OtlParallelExceptions
Exception handling in high-level OTL constructs.

### 49_FramedWorkers
Multiple frames each communication with own worker task.

### 50_OmniValueArray
Wrapping arrays, hashes and records in TOmniValue.

### 51_PipelineStressTest
Pipeline stress test by [Anton Alisov].

### 52_BackgroundWorker
Demo for the Parallel.BackgroundWorker abstraction.

### 53_AsyncAwait

Demo for the Async/Await abstraction.

### 54_LockManager

Lock manager (IOmniLockManager<K>) demo.

 55_ForEachProgress

Demonstrates progress bar updating from a ForEach loop.

### 56_RunInvoke

Simplified 'run & invoke' low-level API.

### 57_For

Simple and fast parallel for.

### 58_ForVsForEach

Speed comparison between Parallel.ForEach, Parallel.For, and TParallel.For (XE7+).

### 59_TWaitFor

Demo for the TWaitFor class.

### 60_Map

Demonstrates the Parallel.Map abstraction.

### 61_CollectionToArray

Demonstrates the TOmniBlockingCollection.ToArray<T> method.

### 62_Console

Demonstrates how to use OmniThreadLibrary from a console application.

### 63_Service

Demonstrates how to use OmniThreadLibrary from a service application.

### 64_ProcessorGroups_NUMA

Demonstrates how to work with processor groups and NUMA nodes.

### 65_TimedTask

Demonstrates the Parallel.TimedTask asbstraction.

### 66_ThreadsInThreads

Demonstrates how to start OmniThreadLibrary threads from background threads.