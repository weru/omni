---
title: "FAQ"
date: 2018-03-07T10:13:28-05:00
draft: false
---
## What is OmniThreadLibrary?
OmniThreadLibrary is a simple to use threading library for Delphi. OTL's main "selling" points (besides the prices, of course ;) are power, simplicity, and openess. With just few lines of code, you can set up multiple threads, send messages between them, process Windows messages and more. OTL doesn't limit you in any way - if it is not powerfull enough for you, you can ignore any part of its "smartness" and replace it with your own code.

## How do I get OmniThreadLibrary?
OTL is hosted on GitHub. [See the Download page for more information.]

## How do I use OmniThreadLibrary?
Download and install the sources. Compile and install the OmniThreadLibrary package. [See the Download page for more information.] Read the tutorials. Examine the demos, which are part of the OmniThreadLibrary download.

## Is OmniThreadLibrary supported on my platform?
At the moment, OTL supports Delphi 2007, 2009, 2010, XE, XE2, XE3, XE4, XE5, XE6, XE7, XE8, 10 Seattle, 10.1 Berlin, and 10.2 Tokyo on Win32 and Win64 platforms using the VCL framework. We are working on support for mobile devices, OS X, and FireMonkey.

## How do I get support?
Support is provided through the Google+ community.

## Your web design sucks!
I fully agree. See How can I contribute below.

## Your logo sucks!
I fully agree. See How can I contribute below.

## How can I contribute?
Use OmniThreadLibrary
Contribute ideas [in the Google+ community]
Spread a good word
Design a prettier site
Create a better logo
OmniThreadLibrary saved my company/marriage/life of my loved ones. How can I repay you?
Buy the Parallel Programming with OmniThreadLibrary book or one of my webinars.

You can also contact me if you have a tough Delphi problem that needs to be solved. I'm not doing databases or reporting, everything else is negotiable.

## Who is Primoz Gabrijelcic, anyway?
I'm a long-time Delphi programmer, writer for Monitor, and Blaise Pascal magazines and frequent contributor to the Delphi-SI community. I'm also the sole author of the The Delphi Geek blog. I like reading SF and working with wood.

