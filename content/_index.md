---
title: 'Home'
IsHome: true
---

## What Is OmniThreadLibrary?
OmniThreadLibrary is simple to use threading library for Delphi. Currently, versions 2007, 2009, 2010, XE, XE2, XE3, XE4, XE5, XE6, XE7, XE8, 10 Seattle, 10.1 Berlin, and 10.2 Tokyo are supported. OmniThreadLibrary is an open source project. It lives on GitHub and is licensed under the BSD license.

This is not the first threading framework I've written. There are, however, few big differences between the OTL (OmniThreadLibrary) and previous projects. All previous projects were closed source, used only for the in-house programming. They were all designed from the bottom. And they all had big problems when used in some real-life cases. Still, they were an important research tools. I learned a lot from them - what works, what doesn't and, most important, what kind of threading issues are appearing in the real life applications.

In the last year I've become more and more unhappy with my previous frameworks and decided that it's time for something better, something simpler. Something built from scratch. This time I didn't start with the implementation but tried to visualize how I'd like to use my framework in real-life applications. After all, I had all those test cases lying around in form of my old applications using old framework. And so, the OmniThreadLibrary was born.

OTL's main "selling" points (besides the price, of course ;) are power, simplicity, and openess. With just few lines of code, you can set up multiple threads, send messages between them, process Windows messages and more. OTL doesn't limit you in any way - if it is not powerfull enough for you, you can ignore any part of its "smartness" and replace it with your own code. You can also ignore the threading part and use only the communication subsystem. Or vice versa. You're the boss.

## How to Start?
Read the FAQ. Read tutorials. Download the code. Check the demos. Watch the webinars. Use it. If in doubt, visit the OTL Google+ community and ask a question.

## Contributors
OTL would not be the same without two Slovenian Delphi hackers - GJ, who wrote the lock-free communication code and Lee_Nover, who found numerous problems in my code and helped with suggestions when I got stuck. Thanks, guys!