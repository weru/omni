---
title: "Download"
date: 2018-03-07T10:10:16-05:00
draft: false
---

## Download

Current version is available as a zipped download.

Snapshots of older versions are also available.

If you want to keep up with the current developments, it's advisable to track the repository instead.

If you have Delphi XE8 or newer, you can install OmniThreadLibrary via GetIt.

If you have Delphi XE or newer, you can install Delphinus package manager and install OmniThreadLibrary there.

## Installation

After you have downloaded (and unpacked) the sources, open packages/OmniThreadLibraryPackages.groupproj, build both packages inside and install the OmniThreadLibraryDesigntime package. This will add component TOmniEventMonitor to the component palete (OmniThreadLibrary page).

